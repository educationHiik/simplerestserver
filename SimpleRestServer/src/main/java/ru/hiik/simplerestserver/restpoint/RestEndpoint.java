/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.simplerestserver.restpoint;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.hiik.entitycore.entity.student.Student;


/**
 *
 * @author vaganovdv
 */

@Path("/students")   // Указание  корня 
public class RestEndpoint
{

    private static final Logger LOG = Logger.getLogger(RestEndpoint.class.getSimpleName());
    
    @GET                    // аннотация операции GET (получить)
    @Path("connected")      // аннотация - имя запроса {connected}
    
    //      возвращаемое значение (экземпляр класса Response)
    //        |     
    public Response isconnected() // Выбор имени метода произвольный, метод без параметров
    {
       String connected = "Connected ==>"+ LocalDateTime.now();
       LOG.log(Level.INFO, connected);
      
       // установка ок (http=200)  контейнер для траспортировки любых классов
       //          |                 |               
       return Response.ok()      .entity(connected).build(); // .build - аналог оператора new
    }
    
    
    
    @GET
    @Path("getAllStudents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllStudents() 
    {
       String list = "Список студентов";
       LOG.log(Level.INFO, list);
       return Response.ok().entity(list).build();
    }
    
    
    
    /**
     * Добавление студента в базу данных
     * @param student
     * @return 
     */
    @PUT
    @Path("addStudent")          
    @Consumes(MediaType.APPLICATION_JSON)   //  Прием     Экземпляра класса Student в формате JSON
    @Produces(MediaType.APPLICATION_JSON)   //  
    
    //                          Экземпляр класса Student
    //                                   |
    public Response addStudent(Student student)    // Имя метода совпадает с URL сервиса REST
    {
       // Добавление фиктивное - вывод фамилии студента
       //                                           | 
       LOG.log(Level.INFO, "Добавлен студент {"+student.getLastName()+"}");
       
       
       // Отправка обратно экземляра класса Student для проверки
       //         |
       return Response.ok().entity(student).build();
    }
    
    
     
    @DELETE
    @Path("deleteStudent/{id}")          
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteStudent(@PathParam("id") long id )    // Имя метода совпадает с URL сервиса REST
    {
       
         LOG.log(Level.INFO, "Удален студент {"+id+"}");
        
       return Response.ok().entity("Студент удален из базы данных {"+id+"}").build();
    }
    
    
}
