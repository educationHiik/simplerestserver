/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.simplerestserver.database;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import static javax.transaction.Transactional.TxType.REQUIRED;

/**
 *
 * @author vaganovdv
 */
@Singleton
public class DatabaseManager
{
     @PersistenceContext(unitName = "STUDENT_DATABASE_PU")
    private EntityManager em;
    
    @Transactional(REQUIRED)
    public void saveStudent(ru.hiik.entitycore.entity.student.Student student)
    {
         em.persist(student);
    }     
    
    
}
